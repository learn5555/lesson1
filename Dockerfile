FROM python:3-alpine

WORKDIR /app
COPY requirenments.txt /app/requirenments.txt 
RUN pip3 install -r requirenments.txt
COPY . .
EXPOSE 8000

ENTRYPOINT ["python3"]
CMD ["manage.py","runserver"," 0.0.0.0:8000"]
